# userapp/urls.py

from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('home/', views.home, name='home'),
    path('profile/', views.profile, name='profile'),
    path('points/', views.points, name='points'),
    path('tasks/', views.tasks, name='tasks'),
    path('logout/', views.user_logout, name='logout'),
    path('signup/', views.signup, name='signup'),
    path('login/', views.user_login, name='login'),
    path('app/<int:app_id>/', views.app_detail, name='app_detail'),
]