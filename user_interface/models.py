from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class UserProfile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	points_earned = models.PositiveIntegerField(default=0)
	tasks_completed = models.PositiveIntegerField(default=0)

	def __str__(self):
		return self.user.username