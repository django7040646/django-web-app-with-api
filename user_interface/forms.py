# user_interface/forms.py

from django import forms
from admin_interface.models import Screenshot

class ScreenshotForm(forms.ModelForm):
    class Meta:
        model = Screenshot
        fields = ['image']
        widgets = {
            'image': forms.FileInput(attrs={'class': 'upload'}),
        }
