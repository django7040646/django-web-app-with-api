from django.shortcuts import render, redirect, get_object_or_404
from admin_interface.models import App
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib import messages
from .models import UserProfile
from admin_interface.models import App
from .forms import ScreenshotForm


def index(request):
    return render(request, 'index.html')

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            # Create a UserProfile when a User is created
            UserProfile.objects.create(user=user)
            messages.success(request, "Signup successful!")
            return redirect('home')  # Redirect to the user's profile page
        else:
            messages.error(request, "Unsuccessful registration. Invalid information.")
    form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})


def user_login(request):
    if request.method == 'POST':
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f"You are now logged in as {username}.")
                return redirect('home')  # Redirect to the user's profile page
            else:
                messages.error(request,"Invalid username or password.")
        else:
            messages.error(request,"Invalid username or password.")
    form = AuthenticationForm()
    return render(request, 'login.html', {'form': form})

@login_required
def home(request):
    apps = App.objects.all()  # Get all app objects from the database
    return render(request, 'home.html', {'apps': apps})

@login_required
def profile(request):
    user_profile = UserProfile.objects.get(user=request.user)
    return render(request, 'profile.html', {'profile': user_profile})

@login_required
def points(request):
    user_profile = UserProfile.objects.get(user=request.user)
    return render(request, 'points.html', {'profile': user_profile})

@login_required
def tasks(request):
    user_profile = UserProfile.objects.get(user=request.user)
    # Assuming there is a Task model related to UserProfile to track tasks
    tasks = user_profile.tasks_completed  # Replace 'task_set' with the related name for tasks
    return render(request, 'tasks.html', {'tasks': tasks})

@login_required
def user_logout(request):
    logout(request)
    return redirect('index')  # Redirect to home page or login page

@login_required
def app_detail(request, app_id):
    app = get_object_or_404(App, pk=app_id)
    if request.method == 'POST':
        form = ScreenshotForm(request.POST, request.FILES)
        if form.is_valid():
            screenshot = form.save(commit=False)
            screenshot.app = app
            screenshot.save()
            messages.success(request, 'Screenshot uploaded successfully!')
            return redirect('app_detail', app_id=app_id)
        else:
            messages.error(request, 'Failed to upload screenshot.')
    else:
        form = ScreenshotForm()
    return render(request, 'app_detail.html', {'app': app, 'form': form})
