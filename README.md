# Problem Set I - Regex

## Overview
This repository contains a Python script `main.py` for demonstrating regex usage to extract numbers from a given text pattern.

### Script
```python
# main.py
import re

text = '{"orders":[{"id":1},{"id":2},...{"id":653}],"errors":[...]}'
pattern = r":(\d+)"
matches = re.findall(pattern, text)
print(matches)
```
# Problem Set III

## Discussion Questions and Answers
### Question A: Scheduling Periodic Tasks
### Answer:
To automate the periodic task of downloading ISINs every 24 hours, using Celery with Django-Celery-Beat is effective. Celery offers robust task scheduling and execution, while Django-Celery-Beat allows managing these tasks directly from the Django admin interface. This setup is reliable and scalable, handling increased workloads by distributing tasks across multiple workers. Potential issues like broker overload or task execution delays under high load can be managed by scaling the broker and optimizing task execution. For simpler applications, Django Background Tasks or APScheduler are viable alternatives, though they may not offer the same level of scalability as Celery.

### Question B: Flask vs. Django
### Answer:
Flask is ideal for small to medium-sized projects, offering flexibility and simplicity, perfect for rapid prototyping and microservices. Its modular design suits beginners and projects requiring custom implementations. Django, in contrast, is tailored for larger, full-featured applications. Its "batteries-included" approach provides built-in functionalities like an ORM, admin panel, and authentication, facilitating rapid development of complex applications. Django's structured framework benefits larger teams and projects, ensuring consistency and scalability. Choose Flask for simpler, smaller-scale projects, and Django for comprehensive, large-scale applications. The decision should align with your project's scale, complexity, and team expertise.

### Question: Automating the Delivery
### Answer:
Automating Django app deployment is effectively achieved through CI/CD pipelines, utilizing platforms like GitHub Actions or Jenkins for tasks including testing, building, and deploying to servers or cloud platforms. This approach streamlines the development workflow, minimizes errors, and ensures consistent, timely updates. Key aspects involve managing environment variables, database migrations, and possibly setting up notifications for build statuses.
