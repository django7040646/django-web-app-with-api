# adminapp/api_views.py

from rest_framework import generics
from .models import App
from .serializers import AppSerializer

class AppListCreate(generics.ListCreateAPIView):
    queryset = App.objects.all()
    serializer_class = AppSerializer
