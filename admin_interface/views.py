from django.shortcuts import render, redirect
from .forms import AppForm
from .models import App
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponse
from .models import AdminUser
from .decorators import admin_login_required


def login_view(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        try:
            user = AdminUser.objects.get(username=username)
            if user.check_password(password) and user.is_active:
                request.session['user_id'] = user.id  # Simple session management
                return redirect('admin_dashboard')
            else:
                return HttpResponse('Invalid credentials or inactive user.')
        except AdminUser.DoesNotExist:
            return HttpResponse('User does not exist.')
    return render(request, 'admin_login.html')

def logout_view(request):
    try:
        del request.session['user_id']  # Remove the user from the session
    except KeyError:
        pass
    return redirect('login_view')

def is_admin(user):
    return user.is_staff

def admin_login(request):
    if request.method == 'POST':
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            user = form.get_user()
            if user.is_staff:  # Only log in staff users
                login(request, user)
                return redirect('admin_home')  # Redirect to the 'add_app' page after login
            else:
                form.add_error(None, 'Only admins can log in here.')
    else:
        form = AuthenticationForm()
    return render(request, 'admin/login.html', {'form': form})

@admin_login_required
def admin_dashboard(request):
    # Your admin dashboard view logic
    return render(request, 'admin/admin_dashboard.html')

@admin_login_required
def new_admin(request):
    # When creating a new user or updating a user's password
    admin_user = AdminUser(username='newadmin')
    admin_user.set_password('plain_password')
    admin_user.save()


@admin_login_required
def admin_home(request):
    apps = App.objects.all()
    return render(request, 'admin/home.html', {'apps':apps})

@admin_login_required
def add_app(request):
    if request.method == 'POST':
        form = AppForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            # Redirect to the app list or success page after saving
            messages.success(request, "The app has been added successfully!")
            return redirect('add_app')
    else:
        form = AppForm()
    return render(request, 'admin/add_app.html', {'form': form})

# def admin_login(request):
#     if request.method=='POST':
#         form = AuthenticationForm(request, data=request.POST)
#         if form.is_valid():
#             username=form.cleaned_data.get('username')
#             password = form.cleaned_data.get('password')
#             user=authenticate(username=username, password=password)
#             if user is not None:
#                 login(request, user)
#                 messages.info(request, f"you are now logged in.")
#                 return redirect('admin/home')
#             else:
#                 messages.error(request, "Invalid usernamer or password")
#         else:
#             messages.error(request, "Invalid usernamer or password")
#     form = AuthenticationForm()
#     return render(request, 'admin/login.html', {'form': form})

# @login_required
# @staff_member_required
# def add_app(request):
#     if request.method == 'POST':
#         form = AppForm(request.POST, request.FILES)
#         if form.is_valid():
#             form.save()
#             # Redirect to the app list or success page after saving
#             messages.success(request, "The app has been added successfully!")
#             return redirect('add_app')
#     else:
#         form = AppForm()
#     return render(request, 'admin/add_app.html', {'form': form})