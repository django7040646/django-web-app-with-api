from django.http import HttpResponse
from functools import wraps

def admin_login_required(view_func):
    @wraps(view_func)
    def _wrapped_view(request, *args, **kwargs):
        if 'user_id' not in request.session:
            return HttpResponse('You need to login first.', status=401)
        return view_func(request, *args, **kwargs)
    return _wrapped_view
