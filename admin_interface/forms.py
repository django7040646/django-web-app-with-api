# admin_interface/forms.py

from django import forms
from .models import App

class AppForm(forms.ModelForm):
    class Meta:
        model = App
        fields = ['name', 'link', 'category', 'sub_category', 'points']
