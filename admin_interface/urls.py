# admin_interface/urls.py

from django.urls import path
from . import views


urlpatterns = [
    path('admin/dashboard/', views.admin_dashboard, name='admin_dashboard'),
    path('admin_home/', views.admin_home, name='admin_home'),
    path('admin/login/', views.login_view, name='login_view'),
    path('admin/logout/', views.logout_view, name='logout_view'),
    path('add_app/', views.add_app, name='add_app'),
]
