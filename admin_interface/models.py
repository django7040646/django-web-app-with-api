from django.db import models
from django.contrib.auth.hashers import make_password, check_password

# Create your models here.

class App(models.Model):
	name = models.CharField(max_length=255)
	link = models.URLField()
	category = models.CharField(max_length=255)
	sub_category = models.CharField(max_length=255)
	points = models.PositiveIntegerField()

	def __str__(self):
		return self.name


class Screenshot(models.Model):
    app = models.ForeignKey(App, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='screenshots/')
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Screenshot for {self.app.name}"


class AdminUser(models.Model):
    username = models.CharField(max_length=150, unique=True)
    password = models.CharField(max_length=128)
    is_active = models.BooleanField(default=True)

    def set_password(self, raw_password):
        self.password = make_password(raw_password)

    def check_password(self, raw_password):
        return check_password(raw_password, self.password)
